### Example Node app in Docker container 

#

#### Create App

```bash
mkdir myfirstapp
cd myfirstapp
```

#

##### create index.js

```js
const express = require('express');
const PORT = 3000;
const app = express();

app.get('/', function (req, res) {
    res.send('Hello World!')
});

app.listen(PORT, function () {
    console.log(`App is running: http://localhost:${PORT}`);
});
```

#

##### create package.json

```
{
    "name": "helloworld",  
    "version": "1.0.0",  
    "description": "Dockerized node.js app",  
    "main": "index.js",  
    "author": "",  
    "license": "ISC",  
    "dependencies": {  
      "express": "^4.16.4"  
    }
 }
```

#

##### create Dockerfile

```docker
FROM node:8  

WORKDIR /app  

COPY package.json /app  

RUN npm install  

COPY . /app  

EXPOSE 3000  

CMD ["node",  "index.js"]
```

___

#### Build Image
```
docker build -t  vinsproduction/myfirstapp .
```
#

#### Run Image
```
docker run -p 8888:3000 --name myfirstapp vinsproduction/myfirstapp
```
#

#### View App

http://localhost:8888

#

#### Push Image
```
docker login

docker push vinsproduction/myfirstapp
```


